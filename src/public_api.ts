/*
 * Public API Surface of admin
 */
export * from './lib/admin.interfaces';
export * from './lib/admin.service';
export * from './lib/admin.component';
export * from './lib/admin.module';
export * from './lib/admin-routing.module';
export * from './lib/users/users-list/users-list.component';
export * from './lib/admin-shared.module';
export * from './lib/permissions/permissions.component';
export * from './lib/privileges/privileges.component';
export * from './lib/default-privileges/default-privileges.component';
export * from './lib/directives/requiredServerPrivileges';
export * from './lib/directives/if-entity.directive';
