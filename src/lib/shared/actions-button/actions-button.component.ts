import { Component, OnInit, Input } from '@angular/core';

export interface ActionButtonItem {
  title: string;
  href?: string;
  click?: any;
  disabled?: boolean;
}

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'admin-actions-button',
  templateUrl: './actions-button.component.html',
  styleUrls: []
})
export class ActionsButtonComponent implements OnInit {

  @Input() actions: any;
  @Input() edit: any;

  constructor() { }

  ngOnInit() {}
}
