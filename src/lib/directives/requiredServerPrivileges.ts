import { Directive, ElementRef, Input } from '@angular/core';
import { PermissionValidationClassBehaviour, RequiredServerPrivileges } from '../admin.interfaces';
import { AdminService } from '../admin.service';


@Directive({
  selector: '[requiredServerPrivileges]',
})
export class RequiredServerPrivilegesDirective {
  constructor(
    private readonly _elementRef: ElementRef,
    private readonly _adminService: AdminService,
  ) {}

  @Input('requiredServerPrivileges') set validatePermissions(
    requiredServerPrivileges: RequiredServerPrivileges
  ) {
    // validate required permissions
    if (
      !(
        Array.isArray(
          requiredServerPrivileges && requiredServerPrivileges.privileges
        ) && requiredServerPrivileges.privileges.length
      )
    ) {
      return;
    }
    (async () => {
      // enumerate permissions
      for (const privilege of requiredServerPrivileges.privileges) {
        try {
          // validate via admin service
          const hasPermission: boolean | null =
            await this._adminService.hasPermission(privilege);
          // if the user does not have access to the element
          // note: explicitly check for false value
          if (
            hasPermission === false &&
            this._elementRef &&
            this._elementRef.nativeElement
          ) {
            switch (
              requiredServerPrivileges.options &&
              requiredServerPrivileges.options.behaviour
            ) {
              case PermissionValidationClassBehaviour.Hide: {
                // hide element
                if (this._elementRef.nativeElement.style) {
                  this._elementRef.nativeElement.style.display = 'none';
                }
                break;
              }
              case PermissionValidationClassBehaviour.Disable: {
                // disable element
                this._elementRef.nativeElement.disabled = true;
                break;
              }
              case PermissionValidationClassBehaviour.Custom: {
                // apply custom class to element
                this._elementRef.nativeElement.classList.add(
                  requiredServerPrivileges.options.customClass
                );
                break;
              }
              default: {
                // hide element by default
                if (this._elementRef.nativeElement.style) {
                  this._elementRef.nativeElement.style.display = 'none';
                }
                break;
              }
            }
            // exit
            return;
          }
        } catch (err) {
          /* Bypass the potential hasPermission error by just logging it,
                     meaning that the default state of the element is to be visible.
                     This is nothing but a fontend validation, the api server will always
                     handle permissions correctly. */
          console.error(err);
        }
      }
    })()
      .then(() => {
        // exit
        return;
      })
      .catch((err) => {
        // log error
        console.error(err);
        // and exit
        return;
      });
  }
}
