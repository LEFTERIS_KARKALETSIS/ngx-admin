import {
  AfterViewInit,
  Directive,
  Input,
  TemplateRef,
  ViewContainerRef
} from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { EdmEntitySet, EdmSchema } from '@themost/client';

// tslint:disable directive-selector
@Directive({
  selector: '[if-entity]'
})
export class IfEntityDirective implements AfterViewInit {
  private $implicit: string;

  constructor(
    private view: ViewContainerRef,
    private template: TemplateRef<any>,
    private context: AngularDataContext
  ) {}

  @Input('if-entity') set assign(value: string) {
    if (value) {
      Object.assign(this, {
        $implicit: value
      });
    }
  }

  public get value() {
    return this.$implicit;
  }

  public set value(value: string) {
    this.$implicit = value;
  }

  ngAfterViewInit(): void {
    // get schema
    this.context
      .getMetadata()
      .then((schema: EdmSchema) => {
        if (schema == null) {
          return;
        }
        // try to find if the related entity set exists
        const entityExists = schema.EntityContainer.EntitySet.find(
          (item: EdmEntitySet) => {
            return item.Name === this.$implicit;
          }
        );
        // create or clear the view accordingly
        !!entityExists
          ? this.view.createEmbeddedView(this.template)
          : this.view.clear();
      })
      .catch((err) => {
        // just log this error
        console.error(err);
      });
  }
}
