// tslint:disable:quotemark
export const PERSONNEL_SEARCH_CONFIG = {
  components: [
    {
      label: 'Columns',
      columns: [
        {
          components: [
            {
              label: 'Admin.Personnel.List.FullName',
              spellcheck: true,
              tableView: true,
              validate: {
                unique: false,
                multiple: false
              },
              key: 'fullName',
              type: 'textfield',
              input: true,
              hideOnChildrenHidden: false
            }
          ],
          width: 6,
          offset: 0,
          push: 0,
          pull: 0
        },
        {
          components: [
            {
              label: 'Admin.Personnel.List.Category',
              labelPosition: 'top',
              dataSrc: 'url',
              data: {
                url: 'PersonnelCategoryTypes?$top=-1',
                headers: []
              },
              template:
                "<span class='d-block pr-5 text-truncate'>{{ item.name }}</span>",
              selectValues: 'value',
              valueProperty: 'alternateName',
              key: 'category',
              type: 'select',
              input: true,
              searchEnabled: false
            }
          ],
          width: 6,
          offset: 0,
          push: 0,
          pull: 0
        }
      ],
      tableView: false,
      key: 'department',
      type: 'columns',
      input: false,
      path: 'columns'
    },
    {
      label: 'Columns',
      columns: [
        {
          components: [
            {
              label: 'Admin.Personnel.List.Status',
              labelPosition: 'top',
              dataSrc: 'url',
              data: {
                url: 'PersonnelStatusTypes?$top=-1',
                headers: []
              },
              template:
                "<span class='d-block pr-5 text-truncate'>{{ item.name }}</span>",
              selectValues: 'value',
              valueProperty: 'alternateName',
              key: 'status',
              type: 'select',
              input: true,
              searchEnabled: false
            }
          ],
          width: 4,
          offset: 0,
          push: 0,
          pull: 0
        },
        {
          components: [
            {
              label: 'Admin.Personnel.List.Username',
              spellcheck: true,
              tableView: true,
              validate: {
                unique: false,
                multiple: false
              },
              key: 'username',
              type: 'textfield',
              input: true,
              hideOnChildrenHidden: false
            }
          ],
          width: 4,
          offset: 0,
          push: 0,
          pull: 0
        },
        {
          components: [
            {
              label: 'Admin.Personnel.List.Email',
              spellcheck: true,
              tableView: true,
              validate: {
                unique: false,
                multiple: false
              },
              key: 'email',
              type: 'textfield',
              input: true,
              hideOnChildrenHidden: false
            }
          ],
          width: 4,
          offset: 0,
          push: 0,
          pull: 0
        }
      ],
      tableView: false,
      key: 'columns1',
      type: 'columns',
      input: false,
      path: 'columns1'
    }
  ],
  criteria: [],
  searches: [],
  model: '',
  defaults: [],
  columns: []
};
