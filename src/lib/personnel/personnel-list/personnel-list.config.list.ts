// tslint:disable:quotemark
export const PERSONNEL_LIST_CONFIG = {
  title: 'Personnel',
  // todo: Change to Personnel when fixed
  model: 'Personnels',
  searchExpression:
    // tslint:disable-next-line:max-line-length
    "indexof(person/familyName, '${text}') ge 0 or indexof(person/givenName, '${text}') ge 0 or indexof(person/email, '${text}') ge 0 or indexof(user/name, '${text}') ge 0",
  columns: [
    {
      name: 'id',
      property: 'id',
      hidden: true
    },
    {
      name: 'person/givenName',
      property: 'givenName',
      hidden: true
    },
    {
      name: 'person/familyName',
      property: 'familyName',
      title: 'Admin.Personnel.List.FullName',
      formatter: 'TemplateFormatter',
      formatString: '${familyName} ${givenName}'
    },
    {
      name: 'category/name',
      property: 'category',
      title: 'Admin.Personnel.List.Category'
    },
    {
      name: 'status/name',
      property: 'status',
      title: 'Admin.Personnel.List.Status'
    },
    {
      name: 'user/name',
      property: 'user',
      title: 'Admin.Personnel.List.Username'
    },
    {
      name: 'person/email',
      property: 'email',
      title: 'Admin.Personnel.List.Email'
    },
    {
      name: 'id',
      formatter: 'ButtonFormatter',
      className: 'text-center',
      virtual: true,
      formatOptions: {
        buttonContent: '<i class="far fa-edit text-indigo"></i>',
        buttonClass: 'btn btn-default',
        commands: [
          {
            outlets: {
              modal: ['${id}', 'edit']
            }
          }
        ]
      }
    }
  ],
  criteria: [
    {
      name: 'fullName',
      filter:
        "(indexof(person/familyName, '${value}') ge 0 or indexof(person/givenName, '${value}') ge 0)",
      type: 'text'
    },
    {
      name: 'category',
      filter: "(category/alternateName eq '${value}')",
      type: 'text'
    },
    {
      name: 'status',
      filter: "(status/alternateName eq '${value}')",
      type: 'text'
    },
    {
      name: 'username',
      filter: "(indexof(user/name, '${value}') ge 0)",
      type: 'text'
    },
    {
      name: 'email',
      filter: "(indexof(person/email, '${value}') ge 0)",
      type: 'text'
    }
  ],
  defaults: {},
  searches: []
};
