import { TableConfiguration } from '@universis/ngx-tables';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { PERSONNEL_LIST_CONFIG } from './personnel-list.config.list';
import { PERSONNEL_SEARCH_CONFIG } from './personnel-list-search.list';

export class PersonnelListConfigurationResolver
  implements Resolve<TableConfiguration> {
  resolve():
    | Observable<TableConfiguration>
    | Promise<TableConfiguration>
    | TableConfiguration {
    return Promise.resolve(PERSONNEL_LIST_CONFIG as TableConfiguration);
  }
}

export class PersonnelListSearchResolver
  implements Resolve<TableConfiguration> {
  resolve():
    | Observable<TableConfiguration>
    | Promise<TableConfiguration>
    | TableConfiguration {
    return Promise.resolve(PERSONNEL_SEARCH_CONFIG as TableConfiguration);
  }
}

export class PersonnelListDefaultConfigurationResolver implements Resolve<any> {
  resolve(): Observable<any> | Promise<any> | any {
    return Promise.resolve(PERSONNEL_LIST_CONFIG as TableConfiguration);
  }
}
