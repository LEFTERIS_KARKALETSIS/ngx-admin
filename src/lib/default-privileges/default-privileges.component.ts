import { Component, Input, OnInit } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { AdminService } from '../admin.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'universis-default-privileges-list',
  templateUrl: './default-privileges.component.html',
})
export class DefaultPrivilegesComponent implements OnInit {
  @Input() entityType: string;
  public defaultPrivileges: any[] = [];
  public isLoading: boolean;

  constructor(
    private readonly _context: AngularDataContext,
    private readonly _adminService: AdminService,
    private readonly _translate: TranslateService
  ) {}

  async ngOnInit() {
    try {
      this.isLoading = true;
      this.defaultPrivileges = await this._context
        .model('Users/DefaultPermissions')
        .save({
          entityType: this.entityType,
        });
      this.defaultPrivileges.forEach((item: { mask: number }) => {
        Object.assign(item, {
          readableMask: this._adminService
            .getConfigurableActions(item.mask)
            .map((action: string) =>
              this._translate.instant(
                `Admin.Permissions.Types.${action.toLowerCase()}`
              )
            )
            .join(', '),
        });
      });
    } catch (err) {
      // just log error
      console.error(err);
    } finally {
      this.isLoading = false;
    }
  }
}
