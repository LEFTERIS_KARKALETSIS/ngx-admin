import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { AppEventService, LoadingService, ToastService } from '@universis/common';
import { RouterModalOkCancel } from '@universis/common/routing';
import { Subscription } from 'rxjs';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'app-remove-user',
  templateUrl: './remove-user.component.html',
  styleUrls: ['./remove-user.component.css']
})
export class RemoveUserComponent extends RouterModalOkCancel implements OnInit, OnDestroy {

  private subscription: Subscription;
  public user: any;
  public lastError: any;

  constructor(
    protected router: Router,
    protected activatedRoute: ActivatedRoute,
    private translateService: TranslateService,
    private context: AngularDataContext,
    private loadingService: LoadingService,
    private toastService: ToastService,
    private appEvent: AppEventService
  ) {
    // call super constructor
    super(router, activatedRoute);
    // set modal title
    this.modalTitle = this.translateService.instant('Admin.Users.RemoveUserAction.Title');
    // and modal class
    this.modalClass = 'modal-lg';
  }

  ngOnInit() {
    this.subscription = this.appEvent.change.subscribe((event) => {
      this.okButtonDisabled = false;
      if (event && event.model === 'User') {
        this.user = event.target;
        if (!(this.user && this.user.enabled)) {
          this.okButtonDisabled = true;
        }
      }
    });
  }

  async ok() {
    try {
      if (!(this.user && this.user.id)) {
        return this.cancel();
      }
      this.loadingService.showLoading();
      // nullify last error
      this.lastError = null;
      // disable and rename user
      await this.context.model(`Users/${this.user.id}/disableAndRename`).save(null);
      // fire app event
      this.appEvent.change.next({
        model: 'Users'
      });
      // show a success toast
      const successToast: {
        Title: string;
        Message: string;
      } = this.translateService.instant('Admin.Users.RemoveUserAction.SuccessToast');
      this.toastService.show(successToast.Title, successToast.Message);
      // and close
      return this.close();
    } catch (err) {
      console.error(err);
      this.lastError = err;
    } finally {
      this.loadingService.hideLoading();
    }
  }

  async cancel() {
    return this.close();
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
