import { Component, AfterViewInit, Input, ViewChild, OnDestroy } from '@angular/core';
import * as USERS_LIST_CONFIG from './users-list.config.list';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { AdvancedTableComponent, AdvancedTableDataResult } from '@universis/ngx-tables';
import { AdvancedSearchFormComponent } from '@universis/ngx-tables';
import {AdvancedTableSearchComponent} from '@universis/ngx-tables';
import {ActivatedTableService} from '@universis/ngx-tables';
import {TemplatePipe} from '@universis/common';

@Component({
  selector: 'lib-users-list',
  templateUrl: './users-list.component.html',
  styles: []
})
export class UsersListComponent implements AfterViewInit, OnDestroy  {

  public readonly config = USERS_LIST_CONFIG;
  private dataSubscription: Subscription;
  private fragmentSubscription: Subscription;
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;
  public recordsTotal: any;
  public actions: any[];
  public edit: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _activatedTable: ActivatedTableService,
  private _template: TemplatePipe) { }

  ngAfterViewInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe( data => {
      this._activatedTable.activeTable = this.table;
      // set search form
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        this.search.ngOnInit();
      }
      // set table config and recall data
      if (data.tableConfiguration) {
        this.table.config = data.tableConfiguration;
        this.advancedSearch.getQuery().then( res => {
          this.table.destroy();
          this.table.query = res;
          this.advancedSearch.text = '';
          this.table.fetch(false);
        });
      }
      this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
        if (fragment && fragment === 'reload') {
          this.table.fetch();
        }
      });
    });
  }



  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
  }

}
