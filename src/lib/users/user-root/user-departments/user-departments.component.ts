import { Component, AfterViewInit, ViewChild} from '@angular/core';
import {AdvancedSelectService, AdvancedTableComponent,AdvancedFilterValueProvider, AdvancedTableConfiguration} from '@universis/ngx-tables';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {departments_list} from './departments';
import {AppEventService, DIALOG_BUTTONS, ErrorService, LoadingService, ModalService, ToastService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import {relativeTime} from 'ngx-bootstrap/chronos/duration/humanize';

@Component({
  selector: 'lib-user-departments',
  templateUrl: './user-departments.component.html'
})
export class UserDepartmentsComponent implements AfterViewInit {

  @ViewChild('departments') departments: AdvancedTableComponent;
  public filter: any = {};
  public user: any;
  public userId: any;
  public usersToBeInserted = 0;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _appEvent: AppEventService,
              private _loadingService: LoadingService,
              private _toastService: ToastService,
              private _advancedFilter: AdvancedFilterValueProvider,
              private _translateService: TranslateService,
              private _errorService: ErrorService,
              private _selectService: AdvancedSelectService,
              private _modalService: ModalService) { }

  ngAfterViewInit() {
    if (this._activatedRoute.parent.snapshot.params.id) {
      this.userId = this._activatedRoute.parent.snapshot.params.id;
    }

    this.user = this._appEvent.change.subscribe(async event => {
      if (event && event.target && event.model === 'User') {
        // reload request
        // tslint:disable-next-line:triple-equals
        if (event.target.id == this.userId) {
          this.user = event.target;

          if (this.user) {
            this.departments.config = AdvancedTableConfiguration.cast(departments_list);
            this.departments.config.model = `Users/${this.user.id}/departments`;
            this.departments.fetch(true);
          }
        }
      }
    });
  }


  async addDepartment() {
    let userDepartmentIds = await this._context
      .model('Users')
      .where('id')
      .equal(this.userId)
      .expand('departments')
      .getItem()
      .then(user => user.departments.map(department => department.id));
    let queryFilter;
    if (userDepartmentIds.length > 0) {
      queryFilter = userDepartmentIds.map(element => `id ne ${element}`).join(' and ');
    } else {
      queryFilter = `id ne -1`;
    }
    this._advancedFilter.values = {
      ...this._advancedFilter.values,
      queryFilter: queryFilter
    };
    this._selectService.select({
      modalTitle: 'Admin.Departments.SelectDepartments',
      tableConfigSrc: './assets/lists/Departments/select.json'
    }).then(async (result) => {
      if (result.result === 'ok') {
        const departments = result.items;
        if (departments && departments.length > 0) {
          const user = await this._context
            .model('Users')
            .where('id')
            .equal(this.userId)
            .expand('departments')
            .getItem();
          const mergedDepartments = [...user.departments, ...departments];
          const newDepartments = mergedDepartments.filter((g, i, arr) => arr.findIndex(t => t.id === g.id) === i);
          this.usersToBeInserted = newDepartments.length - user.departments.length;
          user.departments = newDepartments;
          if (this.usersToBeInserted === 0) {
            this._toastService.show(
              this._translateService.instant('Admin.Departments.AddDepartments.Title'),
              this._translateService.instant(
                'Admin.Departments.AddDepartments.AllAlreadyExist'
              )
            );
            return;
          }
          // save user
          return this._context
            .model('Users')
            .save(user)
            .then(() => {
              this._toastService.show(
                this._translateService.instant('Admin.Departments.AddDepartments.Title'),
                this._translateService.instant(
                  this.usersToBeInserted === 1
                    ? 'Admin.Departments.AddDepartments.One'
                    : 'Admin.Departments.AddDepartments.Many',
                  {value: this.usersToBeInserted}
                )
              );
              // refresh the table
              this.departments.fetch(true);
            })
            .catch((err) => {
              this._errorService.showError(err, {
                continueLink: '.',
              });
            });
        }
      }
    });
  }

  removeDepartment() {
    if (this.departments && this.departments.selected && this.departments.selected.length) {
      const user = {id: this.userId, departments: []};
      const items = this.departments.selected.map( item => {
        // set state for deletion.
        Object.defineProperty(item, '$state', {
          configurable: true,
          writable: true,
          enumerable: true,
          value: 4
        });
        return item;
      });
      user.departments = items;
      // get confirmation.
      this._modalService.showWarningDialog(
        this._translateService.instant('Admin.Users.RemoveDepartments.Title'),
        this._translateService.instant('Admin.Users.RemoveDepartments.Message'),
        DIALOG_BUTTONS.OkCancel).then( result => {
        if (result === 'ok') {
          // save-remove items.
          this._context.model('Users').save(user).then( () => {
            // inform user.
            this._toastService.show(
              this._translateService.instant('Admin.Users.RemoveDepartments.Title'),
              this._translateService.instant((items.length === 1 ?
                'Admin.Users.RemoveDepartments.One' : 'Admin.Users.RemoveDepartments.Many')
                , { value: items.length })
            );
            // fetch users.
            this.departments.fetch();
          }).catch( err => {
            this._errorService.showError(err, {
              continueLink: '.'
            });
          });
        }
      });
    }
  }
}
