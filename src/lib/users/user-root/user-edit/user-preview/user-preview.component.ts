import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'lib-user-preview',
  templateUrl: './user-preview.component.html'
})
export class UserPreviewComponent implements OnInit {

  @Input() user: any;

  constructor() { }

  ngOnInit() {
  }

}
