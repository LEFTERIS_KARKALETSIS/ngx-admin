import {Component, ViewChild, AfterViewInit} from '@angular/core';
import {AdvancedSelectService, AdvancedTableComponent, AdvancedTableConfiguration, TableConfiguration} from '@universis/ngx-tables';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {group_list} from '../../../groups/groups-list/list';
import {AppEventService, DIALOG_BUTTONS, ErrorService, LoadingService, ModalService, ToastService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';


@Component({
  selector: 'lib-user-groups',
  templateUrl: './user-groups.component.html'
})
export class UserGroupsComponent implements AfterViewInit {

  public user: any;
  public userId: any;
  public usersToBeInserted = 0;
  @ViewChild('groups') groups: AdvancedTableComponent;
  public filter: any = {};
  private changeSubscription: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _appEvent: AppEventService,
              private _loadingService: LoadingService,
              private _toastService: ToastService,
              private _translateService: TranslateService,
              private _errorService: ErrorService,
              private _selectService: AdvancedSelectService,
              private _modalService: ModalService) { }

  ngAfterViewInit() {
    if (this._activatedRoute.parent.snapshot.params.id) {
      this.userId = this._activatedRoute.parent.snapshot.params.id;
    }

    this.changeSubscription = this._appEvent.change.subscribe( event => {
      if (event && event.target && event.model === 'User') {
        // reload request
        // tslint:disable-next-line:triple-equals
        if (event.target.id == this.userId) {
          this.user = event.target;
          if (this.user) {
            this.groups.config = AdvancedTableConfiguration.cast(group_list, true);
            this.groups.config.selectable = true;
            this.groups.config.model = `Users/${this.user.id}/groups`;
            this.groups.fetch(true);
          }
        }
      }
    });
  }

  addGroup() {
    this._selectService.select({
      modalTitle: 'Admin.Groups.SelectGroups',
      tableConfigSrc: './assets/lists/Groups/select.json'
    }).then(async (result) => {
      if (result.result === 'ok') {
        const groups = result.items;
        if (groups && groups.length > 0) {
          const user = await this._context
            .model('Users')
            .where('id')
            .equal(this.userId)
            .expand('groups')
            .getItem();
          const mergedGroups = [ ...user.groups, ...groups];
          const newGroups = mergedGroups.filter((g, i, arr) => arr.findIndex(t => t.id === g.id) === i);
          this.usersToBeInserted = newGroups.length - user.groups.length;
          user.groups = newGroups;
          if (this.usersToBeInserted === 0) {
            this._toastService.show(
              this._translateService.instant('Admin.Groups.AddGroups.Title'),
              this._translateService.instant(
                'Admin.Groups.AddGroups.AllAlreadyExist'
              )
            );
            return;
          }
          // save user
          return this._context
            .model('Users')
            .save(user)
            .then((userRes) => {
              this._toastService.show(
                this._translateService.instant('Admin.Groups.AddGroups.Title'),
                this._translateService.instant(
                  this.usersToBeInserted === 1
                    ? 'Admin.Groups.AddGroups.One'
                    : 'Admin.Groups.AddGroups.Many',
                  { value: this.usersToBeInserted }
                )
              );
              // refresh the table
              this.groups.fetch(true);
            })
            .catch((err) => {
              this._errorService.showError(err, {
                continueLink: '.',
              });
            });
        }
      }
    });
  }

  removeGroup() {
    if (this.groups && this.groups.selected && this.groups.selected.length) {
      const user = {id: this.userId, groups: []};
      const items = this.groups.selected.map( item => {
        // set state for deletion.
        Object.defineProperty(item, '$state', {
          configurable: true,
          writable: true,
          enumerable: true,
          value: 4
        });
        return item;
      });
      user.groups = items;
      // get confirmation.
      this._modalService.showWarningDialog(
        this._translateService.instant('Admin.Users.RemoveGroups.Title'),
        this._translateService.instant('Admin.Users.RemoveGroups.Message'),
        DIALOG_BUTTONS.OkCancel).then( result => {
        if (result === 'ok') {
          // save-remove items.
          this._context.model('Users').save(user).then( () => {
            // inform user.
            this._toastService.show(
              this._translateService.instant('Admin.Users.RemoveGroups.Title'),
              this._translateService.instant((items.length === 1 ?
                'Admin.Users.RemoveGroups.One' : 'Admin.Users.RemoveGroups.Many')
                , { value: items.length })
            );
            // fetch users.
            this.groups.fetch();
          }).catch( err => {
            this._errorService.showError(err, {
              continueLink: '.'
            });
          });
        }
      });
    }
  }
}
