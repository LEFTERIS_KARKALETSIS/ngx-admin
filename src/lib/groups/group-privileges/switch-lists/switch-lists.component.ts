import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'app-switch-lists',
  templateUrl: './switch-lists.component.html',
  styleUrls: ['./switch-lists.component.scss']
})
export class SwitchListsComponent implements OnInit {

  @Input() radioOptions: any[] = [];
  @Output() listSelection: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }
}
