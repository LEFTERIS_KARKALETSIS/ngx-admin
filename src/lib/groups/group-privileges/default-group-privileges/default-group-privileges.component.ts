import {
  AfterViewInit,
  Component,
  Input,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult,
  AdvancedTableEditorDirective,
} from '@universis/ngx-tables';
import { Subscription } from 'rxjs';
import { DEFAULT_GROUP_PRIVILEGES_LIST } from './default-group-privileges';
import { AdminService } from '../../../admin.service';
import { LoadingService, ModalService } from '@universis/common';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'universis-default-group-privileges',
  templateUrl: './default-group-privileges.component.html',
})
export class DefaultGroupPrivilegesComponent
  implements AfterViewInit, OnDestroy {
  public tableConfiguration = AdvancedTableConfiguration.cast(
    DEFAULT_GROUP_PRIVILEGES_LIST
  );
  @Input() account: number;
  public recordsTotal: number;
  public lastError: any;
  private subscription: Subscription;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild(AdvancedTableEditorDirective)
  tableEditor: AdvancedTableEditorDirective;
  constructor(
    private _adminService: AdminService,
    private _modalService: ModalService,
    private _loadingService: LoadingService
  ) {}

  async ngAfterViewInit() {
    try {
      this._loadingService.showLoading();
      if (this.account == null) {
        return;
      }
      // clear last error
      this.lastError = null;
      // set model
      this.tableConfiguration.model = `Accounts/${this.account}/DefaultPermissions`;
      // set table config
      this.table.config = this.tableConfiguration;
      // get default permissions
      const defaultPermissions = await this._adminService.getDefaultPermissions(
        this.account
      );
      // init table
      this.table.ngOnInit();
      // and set items
      this.tableEditor.set(defaultPermissions);
    } catch (err) {
      this.lastError = err;
      console.error(err);
    } finally {
      this._loadingService.hideLoading();
    }
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  cancel() {
    // close
    if (this._modalService.modalRef) {
      return this._modalService.modalRef.hide();
    }
  }
}
