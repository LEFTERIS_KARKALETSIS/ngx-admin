import { AdvancedColumnFormatter } from '@universis/ngx-tables';
import { AdminService } from '../../admin.service';
import { TranslateService } from '@ngx-translate/core';

export class PermissionMaskFormatter extends AdvancedColumnFormatter {
  public render(data: any, type: any, row: any, meta: any) {
    if (this.injector && typeof data === 'number') {
      const adminService = this.injector.get(AdminService);
      const translateService = this.injector.get(TranslateService);
      return adminService
        .getConfigurableActions(data)
        .map((action: string) =>
          translateService.instant(
            `Admin.Permissions.Types.${action.toLowerCase()}`
          )
        )
        .join(', ');
    }
    return data;
  }

  constructor() {
    super();
  }
}
