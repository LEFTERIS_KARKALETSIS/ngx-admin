/* tslint:disable:quotemark object-literal-key-quotes object-literal-sort-keys trailing-comma */
export const group_list = {
  "title": "Admin.Groups.All",
  "model": "Groups",
  "searchExpression": "indexof(alternateName, '${text}') ge 0 or indexof(name, '${text}') ge 0",
  "selectable": false,
  "multipleSelect": true,
  "columns": [
    {
      "name": "id",
      "property": "id",
      "formatter": "ButtonFormatter",
      "className": "text-center",
      "formatOptions": {
        "buttonContent": "<i class=\"far fa-eye text-indigo\"></i>",
        "buttonClass": "btn btn-default",
        "commands": ["../../../groups", "${id}", "edit"]
      }
    },
    {
      "name": "name",
      "property": "name",
      "title": "Admin.Groups.Name"
    },
    {
      "name": "alternateName",
      "property": "alternateName",
      "title": "Admin.Groups.AlternateName"
    },
    {
      "name": "description",
      "property": "description",
      "title": "Admin.Users.Description"
    },
    {
      "name":"scopes",
      "virtual": true,
      "property": "scopes",
      "title":"Admin.Groups.Scopes",
      "formatters": [
        {
          "formatter": "TemplateFormatter",
          "formatString": "${scopes.map((scope) => '<span title=\"' + scope + '\" style=\"max-width:164px\" class=\"mr-1 p-2 text-truncate badge badge-gray-100\">' + scope + '</span>').join('')}"
        }
      ]
    },
    {
      "name": "dateCreated",
      "property": "dateCreated",
      "title": "Admin.Users.DateCreated",
      "formatters": [
        {
          "formatter": "DateTimeFormatter",
          "formatString": "short"
        }
      ]
    },
    {
      "name": "dateModified",
      "property": "dateModified",
      "title": "Admin.Users.DateModified",
      "formatters": [
        {
          "formatter": "DateTimeFormatter",
          "formatString": "short"
        }
      ]
    }
  ],
  "defaults": {
    "expand": ["scopes", "tags"]
  },
  "criteria": [
    {
      "name": "alternateName",
      "filter": "(indexof(alternateName, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "name",
      "filter": "(indexof(name, '${value}') ge 0)",
      "type": "text"
    }
  ]
};
