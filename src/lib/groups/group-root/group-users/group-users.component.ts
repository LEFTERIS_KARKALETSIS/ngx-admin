import {Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild, AfterViewInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {
  ActivatedTableService, AdvancedRowActionComponent,
  AdvancedTableComponent, AdvancedSelectService,
  AdvancedTableConfiguration, AdvancedTableDataResult
} from '@universis/ngx-tables';

import {users_list}  from './users';
import {AppEventService, DIALOG_BUTTONS, ErrorService, LoadingService, ModalService, ToastService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import {Observable, Subscription} from 'rxjs';
import {ClientDataQueryable, EdmEntityType, EdmNavigationProperty, EdmProperty, EdmSchema} from '@themost/client';


@Component({
  selector: 'lib-group-users',
  templateUrl: './group-users.component.html'
})
export class GroupUsersComponent implements AfterViewInit, OnDestroy {

  public group: any;
  public groupId: any;
  public usersToBeInserted = 0;
  @ViewChild('members') members: AdvancedTableComponent;
  public filter: any = {};
  private schema: EdmSchema;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _appEvent: AppEventService,
              private _loadingService: LoadingService,
              private _toastService: ToastService,
              private _translateService: TranslateService,
              private _errorService: ErrorService,
              private _selectService: AdvancedSelectService,
              private _activatedTable: ActivatedTableService,
              private _modalService: ModalService
  ) {
  }


  public recordsTotal: any;
  public description;
  any;
  private dataSubscription: Subscription;
  private paramSubscription: Subscription;
  private selectedItems = [];
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();

  ngAfterViewInit() {
    // get schema (do not await this call)
    this._context.getMetadata().then((schema: EdmSchema) => {
      if (schema) {
        this.schema = schema;
      }
    }).catch((err) => {
      // just log the error
      console.error(err);
    });
    if (this._activatedRoute.parent.snapshot.params.id) {
      this.groupId = this._activatedRoute.parent.snapshot.params.id;
    }
    this.group = this._appEvent.change.subscribe(event => {
      if (event && event.target && event.model === 'Group') {
        // reload request
        this.recordsTotal = 0;
        // tslint:disable-next-line:triple-equals
        if (event.target.id == this.groupId) {
          this.group = event.target;
          this.members.config = AdvancedTableConfiguration.cast(users_list);
          this.members.config.model = `Groups/${this.group.id}/members`;
          this.members.fetch();
        } else {
          // load group
          this.group = this._context.model('Groups').where('id').equal(event.target.id).getItem().then(result => {
            this.group = result;
            this.members.config = AdvancedTableConfiguration.cast(users_list);
            this.members.config.model = `Groups/${this.group.id}/members`;
            this.members.fetch();
          });
        }
      }
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }

  async getSelectedItems() {
    let items = [];
    if (this.members && this.members.lastQuery) {
      const lastQuery: ClientDataQueryable = this.members.lastQuery;
      if (lastQuery != null) {
        if (this.members.smartSelect) {
          // get items
          const selectArguments = ['id', 'name', 'alternateName'];
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.members.unselected && this.members.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter(item => {
              return this.members.unselected.findIndex((x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.members.selected.map((item) => {
            return {
              id: item.id,
              name: item.name,
              alternateName: item.alternateName

            };
          });
        }
      }
    }
    return items;
  }

  async removeUser() {
    let items = await this.getSelectedItems();
    if (items && items.length) {
      const group = {id: parseInt(this.groupId, 10), $state: 4};
      items = items.map(item => {
        item.groups = [group];
        return item;
      });
      // get confirmation.
      this._modalService.showWarningDialog(
        this._translateService.instant('Admin.Groups.RemoveUsers.Title'),
        this._translateService.instant('Admin.Groups.RemoveUsers.Message'),
        DIALOG_BUTTONS.OkCancel).then(result => {
        if (result === 'ok') {
          // save-remove items.
          this._context.model('Users').save(items).then(() => {
            // inform user.
            this._toastService.show(
              this._translateService.instant('Admin.Groups.RemoveUsers.Title'),
              this._translateService.instant((items.length === 1 ?
                'Admin.Groups.RemoveUsers.One' : 'Admin.Groups.RemoveUsers.Many')
                , {value: items.length})
            );
            // fetch users.
            this.members.fetch();
          }).catch(err => {
            this._errorService.showError(err, {
              continueLink: '.'
            });
          });
        }
      });
    }
  }

  async addUser() {
    this._selectService.select({
      modalTitle: 'Admin.Users.SelectUsers',
      tableConfigSrc: './assets/lists/Users/select.json'
    }).then(async (result) => {
      if (result.result === 'ok') {
        const selected = result.items;
        let items = [];
        if (selected && selected.length > 0) {
          items = selected.map(async (user) => {
            // get user id.
            const selectedUserId = user.id;
            // get selected user.
            const selectedUser = await this._context
              .model('Users')
              .where('id')
              .equal(selectedUserId)
              .expand('groups')
              .getItem();
            // get user groups.
            const userGroups = selectedUser.groups;
            // find group index.
            const groupIndex = userGroups.findIndex(
              (someGroup) => someGroup.id === this.groupId
            );
            // if the group does not already exist, a new user is to be added.
            if (groupIndex < 0) {
              this.usersToBeInserted++;
            }
            const group = {id: this.groupId};
            return {
              id: user.id,
              groups: [group]
            };
          });

          // execute promises and get users to be saved.
          const usersToBeSaved = await Promise.all(items);
          // if there are no new users to be inserted, inform and close.
          if (this.usersToBeInserted === 0) {
            this._toastService.show(
              this._translateService.instant('Admin.Groups.AddUsers.Title'),
              this._translateService.instant(
                'Admin.Groups.AddUsers.AllAlreadyExist'
              )
            );
            return null;
          }

          // save users.
          return this._context
            .model('Users')
            .save(usersToBeSaved)
            .then(() => {
              this._toastService.show(
                this._translateService.instant('Admin.Groups.AddUsers.Title'),
                this._translateService.instant(
                  this.usersToBeInserted === 1
                    ? 'Admin.Groups.AddUsers.One'
                    : 'Admin.Groups.AddUsers.Many',
                  {value: this.usersToBeInserted}
                )
              );
              // refresh the table
              this.members.fetch(true);
            })
            .catch((err) => {
              this._errorService.showError(err, {
                continueLink: '.',
              });
            });
        }
      }
    });
  }

  async insertAsPersonnel() {
    try {
      if (this.schema == null) {
        throw new Error('The application schema has not been loaded');
      }
      this._loadingService.showLoading();
      // get selected items
      const items = await this.getSelectedItems();
      if (!this.members.smartSelect) {
        // prepare a promise chain
        const asyncMap = items.map(async (item) => {
          // todo: change model to Personnel when fixed
          const personnel = await this._context.model('Personnels')
            .where('user').equal(item.id)
            .select('id')
            .getItem();
          return Object.assign(item, {
            personnel
          });
        });
        // execute promise chain and filter out already set personnel
        this.selectedItems = (await Promise.all(asyncMap)).filter((item) => {
          return item.personnel == null;
        });
      } else {
        // get all personnel data
        // todo: change model to Personnel when fixed
        const personnelData = await this._context.model('Personnels')
          .asQueryable()
          .select('user')
          .take(-1)
          .getItems();
        this.selectedItems = items.filter((item) => {
          const hasPersonnel = personnelData.find((personnelItem: any) => {
            return personnelItem.user === item.id;
          });
          return !hasPersonnel;
        });
      }
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Admin.Groups.InsertAsPersonnelAction.TitleLong',
          description: 'Admin.Groups.InsertAsPersonnelAction.Description',
          refresh: this.refreshAction,
          execute: this.executeInsertAsPersonnel(),
          formTemplate: this.selectedItems && this.selectedItems.length > 0 ? 'Users/insertAsPersonnel' : null,
        }
      });
    } catch (err) {
      this._errorService.showError(err, {
        continueLink: '.'
      });
    } finally {
      this._loadingService.hideLoading();
    }
  }

  executeInsertAsPersonnel() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      this.refreshAction.emit({
        progress: 1
      });
      // get data from modal component
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component &&
        component.formComponent &&
        component.formComponent.form &&
        component.formComponent.form.formio &&
        component.formComponent.form.formio.data;
      if (data == null) {
        throw new Error('Personnel data could not be determined');
      }
      // execute promises in series within an async method
      (async () => {
        // find person entity type
        const personEntityType = this.schema.EntityType.find((entityType) => {
          return entityType.Name === 'Person';
        });
        // and extract its properties
        const personProperties: (EdmProperty | EdmNavigationProperty)[] =
          personEntityType ? [...personEntityType.Property, ...personEntityType.NavigationProperty] : [];
        const relatedEntities: {entitySet: string, properties: (EdmProperty | EdmNavigationProperty)[]} [] = [];
        let personNavigationProperty = null;
        // if person data are to be transfered
        if (data.transferExistingPerson === true) {
          // get current group attributes
          const group = await this._context.model('Groups')
            .where('id').equal(this.groupId)
            .expand('groupAttributes')
            .getItem();
          if (group && Array.isArray(group.groupAttributes) && group.groupAttributes.length > 0) {
            // enumerate group attributes
            for (const attribute of group.groupAttributes) {
              // try to find if a related entity type exists
              const relatedEntityType = this.schema.EntityType.find((entityType) => {
                return entityType.Name === attribute;
              });
              // if it does not, continue
              if (relatedEntityType == null) {
                continue;
              }
              // search for the user navigation property
              const userNavigationProperty = relatedEntityType.NavigationProperty.find((property: any) => {
                return property.Name === 'user';
              });
              // if it does not exist, continue
              if (userNavigationProperty == null) {
                continue;
              }
              // also search for the person navigation property
              personNavigationProperty = relatedEntityType.NavigationProperty.find((property: any) => {
                return property.Name === 'person';
              });
              // if it does not exist, continue
              if (personNavigationProperty == null) {
                continue;
              }
              // find entity set of entity type
              const entitySet = this.schema.EntityContainer.EntitySet.find((item) => {
                return item.EntityType === attribute;
              });
              if (entitySet == null) {
                continue;
              }
              // and push it to the related entities collection
              relatedEntities.push({
                entitySet: entitySet.Name,
                properties: [...relatedEntityType.Property, ...relatedEntityType.NavigationProperty]
              });
            }
          }
        }
        // enumerate selected items
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            let person: any = null;
            for (const entity of relatedEntities) {
              // fetch the related entity object
              // note: safely filter user and expand person
              const entityObject = await this._context.model(entity.entitySet)
                .where('user').equal(item.id)
                .expand('person')
                .getItem();
              if (!(entityObject && entityObject.person)) {
                continue;
              }
              // transfer person data from entity
              person = entityObject.person;
              // if the type of the property is not Person
              if (personNavigationProperty.Type !== 'Person') {
                // enumerate all Person entity properties
                personProperties.forEach((property) => {
                  const propertyName = property.Name;
                  // if the related entity's person does not contain the property
                  if (Object.prototype.hasOwnProperty.call(person, propertyName) === false) {
                    // and it is contained at the base entity object
                    const baseEntityProperty = entity.properties.find((entityProperty) => {
                      return entityProperty.Name === propertyName;
                    });
                    if (baseEntityProperty) {
                      // transfer it to the new person
                      Object.defineProperty(person, propertyName, {
                        configurable: true,
                        enumerable: true,
                        writable: true,
                        value: entityObject[propertyName]
                      });
                    }
                  }
                });
                // delete the id
                delete person.id;
              }
              // and break
              break;
            }
            // if person is not set
            if (person == null) {
              // construct family and given names from user alternateName
              let familyName = this._translateService.instant('Admin.Unknown'),
                  givenName = this._translateService.instant('Admin.Unknown');
              const alternateName = item.alternateName && item.alternateName.split(' ');
              if (Array.isArray(alternateName)) {
                familyName = alternateName[0];
                alternateName.shift();
                givenName = alternateName.join(' ');
              }
              // create a minimal person object
              person = {
                familyName,
                givenName,
                gender: {
                  alternateName: 'other'
                }
              };
            }
            // save a new personnel
            const personnel = {
              person,
              user: item.id,
              category: data.category,
              status: {
                alternateName: data.status
              }
            };
            // todo: change model to Personnel when fixed
            await this._context.model('Personnels').save(personnel);
            result.success += 1;
          } catch (err) {
            // log error
            console.error(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }
}
