import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AdvancedFormComponent} from '@universis/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {AppEventService, TemplatePipe, ToastService} from '@universis/common';

@Component({
  selector: 'lib-group-edit',
  templateUrl: './group-edit.component.html'
})
export class GroupEditComponent implements OnInit {
  public group: any;
  public groupId: any;
  @Input() data: any;
  @Input() src: any;
  @Input() continueLink: any;
  @ViewChild('form') form: AdvancedFormComponent;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _appEvent: AppEventService,
              private _toastService: ToastService,
              private _router: Router,
              private _template: TemplatePipe) { }

  async ngOnInit() {
    if (this._activatedRoute.parent.snapshot.params.id) {
      this.groupId = this._activatedRoute.parent.snapshot.params.id;
    }
    this.src = 'Groups/new';
    this.data = {};

    this.group = this._appEvent.change.subscribe(event => {
      if (event && event.target && event.model === 'Group') {
        // reload request
        // tslint:disable-next-line:triple-equals
        if (event.target.id == this.groupId) {
          this.group = event.target;
          if (this.group) {
            this.src = `Groups/edit`;
            this.data = this.group;
          }
        }
      }
    });
  }

  async onCompletedSubmission($event: any) {
    let continueLink;
    if (this.continueLink && Array.isArray(this.continueLink)) {
      continueLink = this.continueLink.map(x => {
        if (typeof x === 'string') {
          return this._template.transform(x, this.form.data);
        }
        return x;
      });
    } else if (typeof this.continueLink === 'string' && this.continueLink.trim().length > 0) {
      continueLink = [this._template.transform(this.continueLink, this.form.data)];
    }
    if (continueLink) {
      await this._router.navigate(continueLink);
      this._toastService.show( $event.toastMessage.title, $event.toastMessage.body );
    } else {
      await this._router.navigate(['../'], {relativeTo: this._activatedRoute});
      this._toastService.show( $event.toastMessage.title, $event.toastMessage.body );
    }
  }
}
