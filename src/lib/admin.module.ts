import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { AdminComponent } from './admin.component';
import { TablesModule } from '@universis/ngx-tables';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { UsersListComponent } from './users/users-list/users-list.component';
import { AdminRoutingModule } from './admin-routing.module';
import {TranslateModule} from '@ngx-translate/core';
import { UsersComponent } from './users/users.component';
import {UserRootComponent} from './users/user-root/user-root.component';
import {CommonModule} from '@angular/common';
import { UserGroupsComponent } from './users/user-root/user-groups/user-groups.component';
import { UserDepartmentsComponent } from './users/user-root/user-departments/user-departments.component';
import { UserEditComponent } from './users/user-root/user-edit/user-edit.component';
import {UserPreviewComponent} from './users/user-root/user-edit/user-preview/user-preview.component';
import {AdvancedFormsModule} from '@universis/forms';
import { GroupsComponent } from './groups/groups.component';
import { GroupsListComponent } from './groups/groups-list/groups-list.component';
import { GroupRootComponent } from './groups/group-root/group-root.component';
import { GroupEditComponent } from './groups/group-root/group-edit/group-edit.component';
import { GroupUsersComponent } from './groups/group-root/group-users/group-users.component';
import { AdminSharedModule } from './admin-shared.module';
import {SharedModule} from '@universis/common';
import {TooltipModule} from 'ngx-bootstrap/tooltip';
import {RouterModalModule} from '@universis/common/routing';
import {ModalModule} from 'ngx-bootstrap/modal';
import {MostModule} from '@themost/angular';
import { PrivilegesComponent } from './privileges/privileges.component';
import { NgArrayPipesModule, NgPipesModule } from 'ngx-pipes';
import { FormsModule } from '@angular/forms';
import { GroupPrivilegesComponent } from './groups/group-privileges/group-privileges.component';
import { UserModificationHistoryComponent } from './users/user-modification-history/user-modification-history.component';
import { ActionsButtonComponent } from './shared/actions-button/actions-button.component';
import { RemoveUserComponent } from './users/remove-user/remove-user.component';
import {SwitchListsComponent} from './groups/group-privileges/switch-lists/switch-lists.component';
import { PersonnelComponent } from './personnel/personnel.component';
import { PersonnelListComponent } from './personnel/personnel-list/personnel-list.component';
@NgModule({
  imports: [
    TablesModule,
    AdminRoutingModule,
    TranslateModule,
    CommonModule,
    AdvancedFormsModule,
    MostModule,
    AdminSharedModule,
    SharedModule,
    TooltipModule,
    ModalModule,
    ProgressbarModule,
    RouterModalModule,
    NgArrayPipesModule,
    NgPipesModule,
    FormsModule
  ],
  declarations: [
    AdminComponent,
    UsersListComponent,
    UserRootComponent,
    UsersComponent,
    UserGroupsComponent,
    UserDepartmentsComponent,
    UserEditComponent,
    UserPreviewComponent,
    GroupsComponent,
    GroupsListComponent,
    GroupRootComponent,
    GroupEditComponent,
    GroupUsersComponent,
    PrivilegesComponent,
    GroupPrivilegesComponent,
    UserModificationHistoryComponent,
    ActionsButtonComponent,
    RemoveUserComponent,
    SwitchListsComponent,
    PersonnelComponent,
    PersonnelListComponent
  ],
  exports: [AdminComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AdminModule {
  //
}
