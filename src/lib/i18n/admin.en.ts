/* tslint:disable:quotemark object-literal-key-quotes object-literal-sort-keys trailing-comma max-line-length */
export const en = {
  "Admin": {
    "Unknown": "Unknown",
    "Close": "Close",
    "Users": {
      "Title": "User",
      "TitlePlural": "Users",
      "Enabled": {
        "true": "Active",
        "false": "Inactive",
        "1": "Active",
        "0": "Inactive",
        "null": ""
      },
      "AlternateName" : "Alternate Name",
      "Name" : "Name",
      "Description" : "Description",
      "DateCreated": "Created At",
      "DateModified": "Modified At",
      "CreatedBy": "Created By",
      "ModifiedBy": "Modified By",
      "LastLogon" : "Last Logon",
      "StatusTitle" : "Status",
      "Preview": "Preview",
      "Edit": "Edit",
      "Id": "Id",
      "LogonCount": "Logon Count",
      "LockoutTime": "Lockout Time",
      "All": "All",
      "New": "Create",
      "NewTitle": "Create User",
      "Export": "Export",
      "Type": "Type",
      "View": "View",
      "Empty": "There are no records",
      "Total": "total",
      "Info": "Info",
      "Add": "Add user",
      "Remove": "Remove user",
      "SelectUsers": "Select Users",
      "RemoveDepartments" : {
        "Title": "Remove department",
        "Message": "You are about to remove selected departments from this user. Do you want to continue?",
        "One": "Department successfully removed",
        "Many": "{{value}} departments successfully removed."
      },
      "RemoveGroups" : {
        "Title": "Remove group",
        "Message": "You are about to remove selected groups from this user. Do you want to continue?",
        "One": "Group successfully removed",
        "Many": "{{value}} groups successfully removed."
      },
      "CreateUser": "Create user",
      "RemoveUserAction": {
        "Title": "Remove user",
        "Description": "The user {{username}} will be renamed (random alpharithmetic string) and their groups/departments will be deleted, but the user will not be removed from the database. The results of this action will appear at the \"Modification history\" tab.",
        "Information": "Please ensure that this action is valid. The user will lose access to all applications. Do you wish to proceed?",
        "SuccessToast": {
          "Title": "Remove user",
          "Message": "The user was removed successfully."
        },
        "ActiveUsers": "Only for active users"
      },
      "CreatedBefore": "Created before",
      "CreatedAfter": "Created after"
    },
    "Groups": {
      "Title" : "Group",
      "TitlePlural": "Groups",
      "AlternateName" : "Alternate Name",
      "Name" : "Name",
      "Edit": "Edit",
      "New": "Create",
      "NewTitle": "Create Group",
      "Add": "Add Group",
      "SelectGroups": "Select Groups",
      "Remove": "Remove",
      "Scopes": "Client scopes",
      "AddGroups" : {
        "Title": "Add group to user",
        "AllAlreadyExist": "the group(s) you selected already belong(s) to the user.",
        "One": "A new group successfully added to the user.",
        "Many": "{{value}} groups were successfully added to the user.",
        "None": "No group selected for add.",
      },
      "AddUsers": {
        "Title": "Add user to group",
        "AllAlreadyExist": "The user(s) you selected already belong(s) to the group.",
        "One": "One user successfully added.",
        "Many": "{{value}} users successfully added"
      },
      "RemoveUsers" : {
        "Title": "Remove users",
        "Message": "You are about to remove selected users from group. Do you want to continue?",
        "One": "One user successfully removed.",
        "Many": "{{value}} users successfully removed."
      },
      "Privileges": "Permissions",
      "Privilege": "Permission",
      "PrivilegesHelp": "The group privileges cannot be modified in order to ensure smooth core system functionality.",
      "DefaultGroupPrivileges": "Default group privileges",
      "PrivilegeError": "An error occured while retrieving privilege data.",
      "PrivilegesListDescription": "The lists below contains only the extra permissions that have been assigned to the group. You can also view the",
      "DefaultGroupPrivilegesLink": "default system privileges",
      "InsertAsPersonnelAction": {
        "Title": "Insert as personnel",
        "TitleLong": "Insert users as personnel",
        "Description": "With this operation you can insert the selected users as personnel members-given that they have not been already inserted-so they can participate in various related actions (e.g. multiple document signatures)."
      }
    },
    "Departments": {
      "Title" : "Departments",
      "AlternativeCode" : "Alternative Code",
      "DepartmentId" : "Department Id",
      "Name" : "Name",
      "City" : "City",
      "Add": "Add department",
      "Remove": "Remove department",
      "SelectDepartments": "Select department",
      "AddDepartments" : {
        "Title": "Add department to user",
        "AllAlreadyExist": "This user already belong(s) to selected department(s).",
        "One": "Department successfully added.",
        "Many": "{{value}} departments successfully added."
      }
    },
    "Permissions": {
      "EditPermissions": "Edit Permissions",
      "Accounts": "Authorized accounts",
      "AccountSingular": "Authorized account",
      "AddAccount": "Add account",
      "RemoveAccount": "Remove account",
      "RemoveAccounts": "Remove accounts",
      "UndoChanges": "Undo changes",
      "UndoChangesMessage": "You are about to undo the current changes. Do you wish to proceed?",
      "ApplyChanges": "Apply changes",
      "ApplyChangesMessage": "You are about to apply the current permission changes. Do you wish to proceed?",
      "ApplyChangesToastMessage": "The permission changes were saved successfully.",
      "Name": "Account name",
      "AdditionalType": "Account type",
      "AlternateName": "AlternateName",
      "Types": {
        "read": "Read",
        "create": "Create",
        "update": "Update",
        "delete": "Delete",
        "execute": "Execute"
      },
      "FeatureUnavailable": "Permission editing is not available in the version you are using.",
      "UniversalTarget": "All elements",
      "Add": "Add Departments",
      "SelectDepartments": "Select Departments",
      "AddDepartments" : {
        "Title": "Add department to user",
        "AllAlreadyExist": "the department(s) you selected already belong(s) to the user.",
        "One": "A new department successfully added to the user.",
        "Many": "{{value}} departments were successfully added to the user.",
        "None": "No department selected for add.",
      },
      "Manage": "Manage",
      "AddPrivilege": "Assign privilege",
      "SelectPrivilege": "Select privileges"
    },
    "ModificationHistory": {
      "Title": "Modification history",
      "Description": "See the modification history of the user record (e.g when the user was disabled).",
      "DateCreated": "Date",
      "CreatedBy": "Modified by",
      "EventTitle": "Description"
    },
    "Labels": {
      "Actions": "Actions"
    },
    "NotSet": "<Not set>",
    "PrivilegesNotFound": {
      "Title": "Unsupported feature",
      "Description": "The data could not be retreived. The api server configuration and/or version does not support managing privileges."
    },
    "And": "and",
    "NoTags": "Without tags",
    "NoPrivileges": "No available privileges found.",
    "AppliesTo": "Data model (system)",
    "PrivilegeDescription": "Privilege description",
    "PrivilegeName": "Privilege name",
    "PrivilegeAlternateName": "Privilege name (system)",
    "PrivilegeTypes": "Configurable actions",
    "PrivilegeCategory": "Category",
    "Filter": "Search",
    "FilterBy": "Search by the related privilege model, name, description or actions...",
    "SelectCategory": "Select a category",
    "InvalidPrivilege": {
      "Title": "Permission management",
      "Description": "The specified permission cannot be found."
    },
    "DefaultPrivileges": "Default privileges for entity {{entityType}}",
    "NoDefaultPrivilegesFound": "No default privileges found.",
    "DefaultPrivilegeTypes": {
      "global": "Global",
      "self": "Specific",
      "globalPlural": "Global",
      "selfPlural": "Specific"
    },
    "PrivilegeType": "Privilege type",
    "PrivilegeFilter": "Specific condition",
    "PrivilegeActions": "Actions",
    "ParentPrivilege": "Parent privilege/entity",
    "Privilege": "Privilege/Entity",
    "Target": "Is applied for objects",
    "AllTargets": "All entity objects",
    "AllSelfTargets": "That meet the condition",
    "DefineActions": "Define the actions",
    "Ok": "Apply",
    "Cancel": "Cancel",
    "PrivilegeError": "An error occured while trying to save the privileges.",
    "MustDefineActions": "You must define at least one action for each one of the listed privileges.",
    "AddPrivileges": {
      "SuccessToast": {
        "Title": "Privilege assignment",
        "Message": "The privilege assignment to the group has been completed successfully."
      }
    },
    "RemovePrivileges": {
      "Title": "Remove privilege",
      "Description": "The operation will remove the privileges from the group.",
      "CompletedWithErrors": {
        "Title": "Completed with errors",
        "Description": {
          "One": "The privilege did not get removed from the group due to an error.",
          "Many": "The privileges did not get removed from the group due to errors."
        }
      }
    },
    "CopySelfPrivilegesAction": {
      "Title": "Import from other group",
      "Description": "The operation will import/copy the specific/special privileges of the selected group to the current group. After completion, you can edit the actions users can perform on the related privilege entities, by using the \"Edit allowed actions\" action.",
      "CompletedWithErrors": {
        "Title": "Completed with errors",
        "Description": {
          "One": "The copy of the privileges failed due to an error."
        }
      },
      "SuccessToast": {
        "Title": "Import privileges from group",
        "Message": "The privileges from the source group have been imported successfully."
      }
    },
    "EditPrivilegeMask": {
      "Title": "Edit allowed actions",
      "Description": "With this action, you can set the allowed actions (Read, Create, Update, Delete, Execute) that the users will be able to perform on the related privilege entities.",
      "CompletedWithErrors": {
        "Title": "Completed with errors",
        "Description": {
          "One": "The allowed privilege actions did not get updated due to an error.",
          "Many": "The allowed privilege actions did not get updated due to an error."
        }
      }
    },
    "Personnel": {
      "Title": "Personnel",
      "List": {
        "Singular": "Personnel member",
        "Plural": "Personnel members",
        "FullName": "Full name",
        "Category": "Category",
        "Username": "Username",
        "Email": "Email",
        "Status": "Status"
      }
    }
  }
};
